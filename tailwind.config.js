module.exports = {
  theme: {
    fontFamily: {
      'raleway': ['Raleway', 'Sans-serif'],
      'sans': ['Open Sans', 'Sans-serif']
    },
    boxShadow: {
            default: '0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)',
      md: '0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)',
      lg: '0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)',
      card: '0 5px 15px rgba(0, 0, 0, 0.08)',
    },
    extend: {
      colors: {
        primary: {
          ligther: '#3f99f3',
          default: '#1e87f0',
          dark: '#0d70d3'
        }
      },
    },
  },
  variants: {},
  plugins: [],
};