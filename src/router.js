import Vue from 'vue'
import Router from 'vue-router'
// Routes down
import Home from '@/views/Home'
import About from '@/views/About'
import Error404 from '@/views/Error404'
import CoinDetail from '@/views/CoinDetail'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/about',
            name: 'about',
            component: About,
        },
        {
            path: '*',
            name: 'error',
            component: Error404,
        },
        {
            path: '/coin/:id',
            name: 'coin-detail',
            component: CoinDetail,
        }
    ]
})